FROM golang:1.12
RUN apt-get update && apt-get -y install ffmpeg
RUN go get -u github.com/bwmarrin/dca/cmd/dca
WORKDIR /entrysound_dir
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build
CMD ["./entrysound_dir"]