package frontend

import (
	"html/template"
	"net/http"
	"path/filepath"
	"sync"
)

type templateHandler interface {
	getTemplateHandle() *templateHandle
}

type templateHandle struct {
	once     sync.Once
	filename string
	templ    *template.Template
}

// IndexHandler is the template handler used for the index page
type IndexHandler struct {
	redirectURI    string
	templateHandle templateHandle
}

type indexPageData struct {
	Request     *http.Request
	RedirectURI string
}

// NewIndexHandler creates a new IndexHandler with a preconfigured templateHandle
func NewIndexHandler(redirectURI string) *IndexHandler {
	return &IndexHandler{templateHandle: templateHandle{filename: "index.html"}, redirectURI: redirectURI}
}

func (ih *IndexHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	initializeTemplate(ih)
	ih.templateHandle.templ.Execute(w, indexPageData{Request: req, RedirectURI: ih.redirectURI})
}

func (ih *IndexHandler) getTemplateHandle() *templateHandle {
	return &ih.templateHandle
}

// RedirectHandler is the template handler used for the redirect page
type RedirectHandler struct {
	templateHandle templateHandle
}

// NewRedirectHandler creates a new RedirectHandler with with a preconfigured templateHandle
func NewRedirectHandler() *RedirectHandler {
	return &RedirectHandler{templateHandle: templateHandle{filename: "redirect.html"}}
}

func (rh *RedirectHandler) getTemplateHandle() *templateHandle {
	return &rh.templateHandle
}

func (rh *RedirectHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	initializeTemplate(rh)
	rh.templateHandle.templ.Execute(w, req)
}

func initializeTemplate(th templateHandler) {
	th.getTemplateHandle().once.Do(func() {
		th.getTemplateHandle().templ = template.Must(template.ParseFiles(filepath.Join("frontend/templates", th.getTemplateHandle().filename)))
	})
}
