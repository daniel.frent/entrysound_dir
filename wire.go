//+build wireinject

package main

import (
	"context"
	"os"

	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"github.com/google/wire"
	"github.com/sirupsen/logrus"
	"google.golang.org/api/option"
)

var confGetterSet = wire.NewSet(
	NewConfigGetter,
	wire.Bind((*UserConfigGetter)(nil), (*FirebaseConfigGetter)(nil)),
	ProvideBucket,
	ProvideServerURLGetter)

func InitializeServer() *server {
	wire.Build(NewServer, NewLogger, confGetterSet)
	return &server{}
}

func NewServer(c UserConfigGetter, l logrus.FieldLogger, b *storage.BucketHandle) *server {
	return &server{
		configGetter: c,
		logger:       l,
		bucket:       b,
	}
}

func NewLogger() logrus.FieldLogger {
	l := logrus.New()
	l.SetOutput(os.Stdout)
	l.SetFormatter(&logrus.JSONFormatter{})
	return l.WithField("app", "entrysound_dir")
}
func NewConfigGetter(logger logrus.FieldLogger, bucket *storage.BucketHandle, baseURL serverURLGetter) *FirebaseConfigGetter {
	return &FirebaseConfigGetter{logger: logger, bucket: bucket, BaseURL: baseURL()}
}

func ProvideBucket(logger logrus.FieldLogger) *storage.BucketHandle {
	config := &firebase.Config{
		StorageBucket: "discord-entrysound.appspot.com",
	}
	opt := option.WithCredentialsFile(*keyFile)
	app, err := firebase.NewApp(context.Background(), config, opt)
	if err != nil {
		panic(err)
	}
	client, err := app.Storage(context.Background())
	if err != nil {
		panic(err)
	}
	bucket, err := client.DefaultBucket()
	if err != nil {
		panic(err)
	}
	return bucket
}

type serverURLGetter func() ServerURL

func ProvideServerURLGetter() serverURLGetter {
	return serverURLGetter(func() ServerURL {
		return ServerURL(*baseURL)
	})
}
