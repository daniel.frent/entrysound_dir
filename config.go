package main

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

type UserConfigGetter interface {
	GetUserConfig(context.Context) (SoundConf, error)
}

type SoundConf struct {
	Sounds []Sound `json:"sounds"`
}

type ServerURL string

type Sound struct {
	UserID   string `json:"user_id"`
	Location string `json:"location"`
	Type     string `json:"type"`
}

type FirebaseConfigGetter struct {
	bucket  *storage.BucketHandle
	logger  logrus.FieldLogger
	BaseURL ServerURL
}

func (c *FirebaseConfigGetter) GetUserConfig(ctx context.Context) (SoundConf, error) {
	var conf = SoundConf{Sounds: make([]Sound, 0)}
	it := c.bucket.Objects(ctx, nil)
	for {
		objAttrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			c.logger.WithError(err).Error("Could not iterate over objects")
			return SoundConf{}, errors.Wrap(err, "could not iterate over next object")
		}
		conf.Sounds = append(conf.Sounds, Sound{
			UserID:   objAttrs.Name,
			Location: fmt.Sprintf("%s/sounds/%s", string(c.BaseURL), objAttrs.Name),
			Type:     "url",
		})
	}
	return conf, nil
}
