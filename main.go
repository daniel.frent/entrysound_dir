package main

import (
	"bytes"
	"context"
	"encoding/json"
	"entrysound_dir/frontend"
	"entrysound_dir/transcoder"
	"flag"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"

	"cloud.google.com/go/storage"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

var (
	keyFile = flag.String("key", "/etc/key.json", "Path to JSON Key File to authenticate with Firebase")
	baseURL = flag.String("base-url", "http://localhost", "Redirect URL for discord OAuth")
)

func main() {
	flag.Parse()
	s := InitializeServer()
	r := mux.NewRouter()
	r.HandleFunc("/mp3", s.HandleUploadMP3)
	r.HandleFunc("/conf", s.HandleGetConf)
	r.HandleFunc("/sounds/{id}", s.HandleGetSound)
	r.HandleFunc("/sounds/{id}/lastUpdated", s.HandleGetSoundInfo)
	r.Handle("/", frontend.NewIndexHandler(*baseURL))
	r.Handle("/redirect", frontend.NewRedirectHandler())
	http.ListenAndServe(":80", r)
}

type server struct {
	configGetter UserConfigGetter
	logger       logrus.FieldLogger
	bucket       *storage.BucketHandle
}

func (s *server) HandleUploadMP3(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	bodyReader := io.LimitReader(r.Body, 102400)
	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		s.handleError(w, err)
		return
	}
	defer r.Body.Close()
	var buf = &bytes.Buffer{}
	err = transcoder.TranscodeMP3ToDCA(body, buf)
	if err != nil {
		s.handleError(w, err)
		return
	}
	accessToken := r.Header.Get("Authorization")
	discord, err := discordgo.New("Bearer " + accessToken)
	if err != nil {
		s.handleError(w, errors.Wrap(err, "could not Open Discord Client"))
		return
	}
	user, err := discord.User("@me")
	if err != nil {
		s.handleError(w, errors.Wrap(err, "could not retrieve User @me"))
		return
	}
	discordUser := user.ID
	writer := s.bucket.Object(discordUser).NewWriter(r.Context())
	_, err = io.Copy(writer, buf)
	if err != nil {
		s.handleError(w, err)
		return
	}
	writer.Close()
	if err != nil {
		s.handleError(w, err)
		return
	}
	go http.Get("http://entrysound-bot/update")
	s.logger.WithField("user_id", discordUser).Info("Wrote DCA to Storage")
	w.WriteHeader(200)
	return
}

func (s *server) HandleGetConf(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	c, err := s.configGetter.GetUserConfig(r.Context())
	if err != nil {
		s.handleError(w, err)
		return
	}
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(c)
}

func (s *server) HandleGetSound(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]
	reader, err := s.bucket.Object(userID).NewReader(r.Context())
	if err == storage.ErrObjectNotExist {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		s.handleError(w, err)
		return
	}
	io.Copy(w, reader)
}

type lastUpdated struct {
	UserID      string    `json:"user_id"`
	LastUpdated time.Time `json:"last_updated"`
}

func (s *server) HandleGetSoundInfo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID := vars["id"]
	objAttrs, err := s.bucket.Object(userID).Attrs(context.Background())
	if err == storage.ErrObjectNotExist {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		s.handleError(w, err)
		return
	}
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(lastUpdated{UserID: userID, LastUpdated: objAttrs.Updated})
}

func (s *server) handleError(w http.ResponseWriter, err error) {
	w.WriteHeader(500)
	s.logger.WithError(err).Error("Error Handling Request")
	return
}
